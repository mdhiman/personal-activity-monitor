import re
import time
import pickle
from subprocess import *
from collections import defaultdict
from random import random

# source:
# http://stackoverflow.com/questions/3983946/get-active-window-title-in-x
def get_active_window_title():
    root = Popen(['xprop', '-root', '_NET_ACTIVE_WINDOW'], stdout=PIPE)

    id_w = None

    for line in root.stdout:
        m = re.search('^_NET_ACTIVE_WINDOW.* ([\w]+)$', line)
        if m != None:
            id_ = m.group(1)
            id_w = Popen(['xprop', '-id', id_, 'WM_NAME'], stdout=PIPE)
            break

    if id_w != None:
        for line in id_w.stdout:
            match = re.match("WM_NAME\(\w+\) = (?P<name>.+)$", line)
            if match != None:
                return match.group("name")

    return "Active window not found"

def monitor_and_write():
    with open("monitor.pickle", "a") as file:
        try:
            window_titles = pickle.load(file)
        except:
            window_titles = defaultdict(int)

        while True:
            t = get_active_window_title()
            window_titles[t] += 1
            pickle.dump(window_titles, file)
            time.sleep(1)

if __name__ == "__main__":
    monitor_and_write()
